<?php

$sRequest = $_SERVER['REQUEST_URI'];
$aParse = explode('/', $sRequest);
if ($aParse[1] != "user") {
    header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
    echo "404";die;
}

$dns = "mysql:host=db;dbname=crush";
$bdd = new PDO($dns, 'root', 'wordpress');

function getAll() {
    global $bdd;

    $query= $bdd->query('SELECT * FROM `User`');
    $aResponse = [];
    while ($response = $query->fetchObject()) {
        $aResponse[] = $response;
    }

    echo json_encode($aResponse);
}

function getById($id) {
    global $bdd;

    $query= $bdd->query("SELECT * FROM `User` WHERE id = $id");
    $aResponse = [];
    while ($response = $query->fetchObject()) {
        $aResponse[] = $response;
    }

    echo json_encode($aResponse);
}

function post($firstName, $lastName, $email) {
    global $bdd;

    $query = $bdd->prepare("INSERT INTO `User` (`firstName`, `lastName`, `email`, `created_date`) VALUES ('$firstName', '$lastName', '$email', now());");
    $query->execute();

    if (!$id = $bdd->lastInsertId()) {
        header($_SERVER["SERVER_PROTOCOL"], true, 409);
        echo "409";die;
    }

    return $id;
}

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    if (isset($aParse[2])) {
        $id = $aParse[2];
        if(!is_numeric($id)) {
            header($_SERVER["SERVER_PROTOCOL"]." 400 	bad input parameter", true, 400);
            echo "400";die;
        }
        getById($id);die;
    }
    getAll();die;
} elseif ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['firstName'], $_POST['lastName'], $_POST['email'])) {
        $id = post($_POST['firstName'], $_POST['lastName'], $_POST['email']);
        http_response_code(201);
        $user = new stdClass();
        $user->id = $id;
        $user->firstName = $_POST['firstName'];
        $user->lastName = $_POST['lastName'];
        $user->email = $_POST['email'];
        $user->date = date("Y-m-d");
        echo json_encode($user);die;
    } else {
        header($_SERVER["SERVER_PROTOCOL"], true, 400);
        echo "400";die;
    }
}

